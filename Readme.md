# Web Trekk Coding Challenge - Rock Paper Scissor #

A command line application which will allow the user/computer to play Rock, paper and scissor game

### What is this repository for? ###

* Has command line interface to play rock, paper and scissor
* The user can chose number of rounds. The number of rounds is equal to sum of number of wins and loses
* The user then has to chose game mode
   - single - Player vs Computer
   - computer - Computer vs Computer
* The user can also chose to play again

### Prerequisites ###

* Java 8
* Maven
* Git  

### How do I get set up? ###

* Clone the project
```
git clone git@bitbucket.org:rthirucs/rock-paper-scissor.git
```
* To run tests

```
mvn clean test

```
* To run the app

```
mvn exec:java -Dexec.mainClass="com.webtrekk.rps.Application"
```
* Sample execution
```
Lets Play Rock Paper Scissor!! Press Ctrl + c to exit
Enter number of rounds in odd number.
3
Please enter the game mode (single or computer):
single
Enter the player name:
Thiru
Please chose your weapon (rock/paper/scissor)
rock
User chose: ROCK
Computer chose: SCISSOR
The winner of the round is Thiru
Please chose your weapon (rock/paper/scissor)
rock
User chose: ROCK
Computer chose: PAPER
The winner of the round is Computer
Please chose your weapon (rock/paper/scissor)
rock
User chose: ROCK
Computer chose: SCISSOR
The winner of the round is Thiru
Computer score: 1
Thiru score: 2
Congrats Thiru!! You are the winner
Do you want to play another game?(yes/no)
no
```
### Pipeline ###

* Created a pipeline in BitBucket, which will run for every commit. [PipeLine](https://bitbucket.org/rthirucs/rock-paper-scissor/addon/pipelines/home#!/)


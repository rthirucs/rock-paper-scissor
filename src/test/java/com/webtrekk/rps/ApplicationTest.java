package com.webtrekk.rps;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class ApplicationTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final TextFromStandardInputStream inputStream = emptyStandardInputStream();

    @Test
    public void shouldInitializeTheGameEngineAndStartTheGame() throws Exception {
        inputStream.provideLines("1", "computer", "no");

        Application.main(new String[]{});

        assertThat(systemOutRule.getLog(), containsString("Congrats"));
        assertThat(systemOutRule.getLog(), containsString("You are the winner"));
    }

    @Test
    public void shouldAskTheUserIfNeedToContinueTheGame() throws Exception {
        inputStream.provideLines("1", "computer", "yes", "1", "computer", "no");

        Application.main(new String[]{});

        assertThat(countMatches(systemOutRule.getLog(), "Do you want to play another game?(yes/no)"), is(2));
        assertThat(countMatches(systemOutRule.getLog(), "Enter number of rounds in odd number."), is(2));
    }
}
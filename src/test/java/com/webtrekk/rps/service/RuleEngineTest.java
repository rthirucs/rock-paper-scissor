package com.webtrekk.rps.service;

import org.junit.Test;

import static com.webtrekk.rps.model.Weapons.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RuleEngineTest {

    @Test
    public void shouldReturnOneWhenTheFirstOperandIsRockAndSecondIsScissor() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(ROCK, SCISSOR);

        assertThat(result, is(1));
    }

    @Test
    public void shouldReturnZeroWhenTheFirstOperandIsRockAndSecondIsAlsoRock() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(ROCK, ROCK);

        assertThat(result, is(0));
    }

    @Test
    public void shouldReturnNegativeWhenTheFirstOperandIsRockAndSecondIsPaper() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(ROCK, PAPER);

        assertThat(result, is(-1));
    }

    @Test
    public void shouldReturnOneWhenTheFirstOperandIsScissorAndSecondIsPaper() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(SCISSOR, PAPER);

        assertThat(result, is(1));
    }

    @Test
    public void shouldReturnZeroWhenTheFirstOperandIsScissorAndSecondIsAlsoScissor() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(SCISSOR, SCISSOR);

        assertThat(result, is(0));
    }

    @Test
    public void shouldReturnNegativeWhenTheFirstOperandIsScissorAndSecondIsRock() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(SCISSOR, ROCK);

        assertThat(result, is(-1));
    }

   @Test
    public void shouldReturnNegativeWhenTheFirstOperandIsPaperAndSecondIsRock() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(PAPER, SCISSOR);

        assertThat(result, is(-1));
    }

    @Test
    public void shouldReturnZeroWhenTheFirstOperandIsPaperAndSecondIsAlsoPaper() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(PAPER, PAPER);

        assertThat(result, is(0));
    }

    @Test
    public void shouldReturnPositiveWhenTheFirstOperandIsPaperAndSecondIsRock() throws Exception {
        RuleEngine ruleEngine = new RuleEngine();

        int result = ruleEngine.apply(PAPER, ROCK);

        assertThat(result, is(1));
    }
}
package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class ComputerPlayersHandlerTest {

    @Mock
    private PointTracker pointTracker;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldReturnTrueWhenTheTypeIsComputer() throws Exception {
        ComputerPlayersHandler handler = new ComputerPlayersHandler(pointTracker);

        boolean canHandle = handler.canHandle("computer");

        assertThat(canHandle, is(true));
    }

    @Test
    public void shouldReturnFalseWhenTheTypeIsNotComputer() throws Exception {
        ComputerPlayersHandler handler = new ComputerPlayersHandler(pointTracker);

        boolean canHandle = handler.canHandle("not computer");

        assertThat(canHandle, is(false));
    }

    @Test
    public void shouldInvokeGameEngineToPlayGameWithTheTwoComputerPlayers() throws Exception {
        ComputerPlayersHandler handler = new ComputerPlayersHandler(pointTracker);

        handler.initializePlayers();

        assertThat(handler.getFirstPlayer().getName(), is("Computer 1"));
        assertThat(handler.getFirstPlayer().getType(), is("computer"));
        assertThat(handler.getSecondPlayer().getName(), is("Computer 2"));
        assertThat(handler.getSecondPlayer().getType(), is("computer"));
    }

    @Test
    public void shouldInvokePointTrackerToAddPlayers() throws Exception {
        ComputerPlayersHandler handler = new ComputerPlayersHandler(pointTracker);
        Player firstPlayer = new PlayerBuilder().setType("computer").setName("Computer 1").build();
        Player secondPlayer = new PlayerBuilder().setType("computer").setName("Computer 2").build();

        handler.initializePlayers();

        verify(pointTracker).addPlayers(firstPlayer, secondPlayer);
    }
}
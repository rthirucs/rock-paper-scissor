package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SinglePlayerHandlerTest {

    private SinglePlayerHandler handler;

    @Mock
    private InputService inputService;

    @Mock
    private PointTracker pointTracker;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        handler = new SinglePlayerHandler(inputService, pointTracker);
    }

    @Test
    public void shouldReturnTrueWhenTheTypeIsSingle() throws Exception {
        boolean canHandle = handler.canHandle("single");

        assertThat(canHandle, is(true));
    }

    @Test
    public void shouldReturnFalseWhenTheTypeIsNotSingle() throws Exception {
        boolean canHandle = handler.canHandle("not single");

        assertThat(canHandle, is(false));
    }

    @Test
    public void shouldInvokeRuleEngineWithThePlayers() throws Exception {
        when(inputService.getPlayerName()).thenReturn("John");

        handler.initializePlayers();

        assertThat(handler.getFirstPlayer().getName(), is("John"));
        assertThat(handler.getFirstPlayer().getType(), is("human"));
        assertThat(handler.getSecondPlayer().getName(), is("Computer"));
        assertThat(handler.getSecondPlayer().getType(), is("computer"));
    }

    @Test
    public void shouldGetNameOfThePlayerFromTheUser() throws Exception {
        handler.initializePlayers();

        verify(inputService).getPlayerName();
    }

    @Test
    public void shouldInvokePointTrackerToAddPlayers() throws Exception {
        when(inputService.getPlayerName()).thenReturn("John");

        Player firstPlayer = new PlayerBuilder()
                .setName("John")
                .setType("human").build();
        Player secondPlayer = new PlayerBuilder()
                .setName("Computer")
                .setType("computer").build();

        handler.initializePlayers();

        verify(pointTracker).addPlayers(firstPlayer, secondPlayer);
    }

}
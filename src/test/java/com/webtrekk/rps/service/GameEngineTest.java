package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;
import com.webtrekk.rps.model.Weapons;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.webtrekk.rps.model.Weapons.*;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class GameEngineTest {

    private GameEngine gameEngine;

    @Mock
    private InputService inputService;

    @Mock
    private RuleEngine ruleEngine;

    @Mock
    private OutputService outputService;

    @Mock
    private PointTracker pointTracker;

    @Mock
    private SinglePlayerHandler singlePlayerHandler;

    @Mock
    private ComputerPlayersHandler computerPlayersHandler;

    @Rule
    public final TextFromStandardInputStream inputStream = emptyStandardInputStream();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        gameEngine = new GameEngine(inputService, ruleEngine, outputService, pointTracker, asList(singlePlayerHandler, computerPlayersHandler));
        Player human = new PlayerBuilder()
                .setName("Thiru")
                .setType("human").build();
        Player computer = new PlayerBuilder()
                .setName("computer")
                .setType("computer").build();
        Map<Player, Integer> map = new HashMap<Player, Integer>() {{
            put(human, 10);
            put(computer, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);
        when(inputService.getNumberOfGames()).thenReturn(1);
        when(singlePlayerHandler.canHandle(any())).thenReturn(true);
        when(singlePlayerHandler.getFirstPlayer()).thenReturn(human);
        when(singlePlayerHandler.getSecondPlayer()).thenReturn(computer);
        when(ruleEngine.apply(any(), any())).thenReturn(1);
    }

    @Test
    public void shouldInvokeInputServiceToFetchInputForToCompute() throws Exception {
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(-1);
        when(inputService.getInput(any())).thenReturn(SCISSOR, PAPER);

        gameEngine.play();

        verify(inputService).getInput("human");
        verify(inputService).getInput("computer");
    }

    @Test
    public void shouldVerifyIfTheRuleEngineIsInvokedToProcessTheInput() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK);
        when(inputService.getInput("computer")).thenReturn(SCISSOR);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(-1);

        gameEngine.play();

        verify(ruleEngine).apply(ROCK, SCISSOR);
    }

    @Test
    public void shouldDeclareHumanAsWinnerWhenTheRuleEngineReturnsOne() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK);
        when(inputService.getInput("computer")).thenReturn(SCISSOR);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(1);

        gameEngine.play();

        ArgumentCaptor<Player> argumentCaptor = ArgumentCaptor.forClass(Player.class);
        verify(outputService).printResult(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is("human"));
    }

    @Test
    public void shouldDeclareComputerAsWinnerWhenTheRuleEngineReturnsNegative() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK);
        when(inputService.getInput("computer")).thenReturn(SCISSOR);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(-1);

        gameEngine.play();

        ArgumentCaptor<Player> argumentCaptor = ArgumentCaptor.forClass(Player.class);
        verify(outputService).printResult(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is("computer"));
    }

    @Test
    public void shouldInvokePointTrackerForEachRoundAndUpdateThePointsOfThePlayer() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK);
        when(inputService.getInput("computer")).thenReturn(SCISSOR);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(1);

        gameEngine.play();

        ArgumentCaptor<Player> argumentCaptor = ArgumentCaptor.forClass(Player.class);
        verify(pointTracker).incrementScore(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is("human"));
    }

    @Test
    public void shouldResetPointsWhenTheGameIsStarted() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK);
        when(inputService.getInput("computer")).thenReturn(SCISSOR);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(1);

        gameEngine.play();

        verify(pointTracker).resetScores();
    }

    @Test
    public void shouldInvokeOutputServiceWhenTheRuleEngineReturns0() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK, SCISSOR);
        when(inputService.getInput("computer")).thenReturn(ROCK, PAPER);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(0, -1);

        gameEngine.play();

        verify(outputService).tieMessage();
    }

    @Test
    public void shouldNotIncrementScoreForTheRoundWhenThereIsATieInTheRoundAndIncrementDuringSecondRound() throws Exception {
        when(inputService.getInput("human")).thenReturn(PAPER, SCISSOR);
        when(inputService.getInput("computer")).thenReturn(PAPER, PAPER);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(0, 1);

        gameEngine.play();

        ArgumentCaptor<Player> argumentCaptor = ArgumentCaptor.forClass(Player.class);
        verify(pointTracker, times(1)).incrementScore(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is("human"));
    }

    @Test
    public void shouldInvokeInputServiceAgainToFetchTheInputWhenTheRuleEngineReturns0ForTheFirstTime() throws Exception {
        when(inputService.getInput("human")).thenReturn(ROCK, SCISSOR);
        when(singlePlayerHandler.canHandle(any())).thenReturn(true);
        when(inputService.getInput("computer")).thenReturn(ROCK, PAPER);
        when(ruleEngine.apply(any(Weapons.class), any(Weapons.class))).thenReturn(0, 1);

        gameEngine.play();

        ArgumentCaptor<Player> argumentCaptor = ArgumentCaptor.forClass(Player.class);
        verify(outputService).printResult(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is("human"));
        verify(inputService, times(2)).getInput("human");
        verify(inputService, times(2)).getInput("computer");
    }

    @Test
    public void shouldInvokeInitializeOfSinglePlayerHandlerWhenTheGameModeIsSingle() throws Exception {
        when(inputService.getGameMode()).thenReturn("single");
        when(singlePlayerHandler.canHandle(any())).thenReturn(true);

        gameEngine.play();

        verify(singlePlayerHandler).initializePlayers();
    }

    @Test
    public void shouldReturnComputerPlayerHandlerWhenTheInputIsComputer() throws Exception {
        when(inputService.getNumberOfGames()).thenReturn(0);
        when(inputService.getGameMode()).thenReturn("computer");
        when(singlePlayerHandler.canHandle(any())).thenReturn(false);
        when(computerPlayersHandler.canHandle(any())).thenReturn(true);

        gameEngine.play();

        verify(computerPlayersHandler).initializePlayers();
    }

    @Test
    public void shouldGetNumberOfGamesFromTheUser() throws Exception {
        when(inputService.getGameMode()).thenReturn("computer");
        Player human = new PlayerBuilder()
                .setName("Thiru")
                .setType("human").build();
        Player computer = new PlayerBuilder()
                .setName("computer")
                .setType("computer").build();
        Map<Player, Integer> map = new HashMap<Player, Integer>() {{
            put(human, 10);
            put(computer, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);

        gameEngine.play();

        verify(inputService).getNumberOfGames();
    }

    @Test
    public void shouldGetTheGameModeForThePlay() throws Exception {
        when(inputService.getGameMode()).thenReturn("computer");
        Player human = new PlayerBuilder()
                .setName("Thiru")
                .setType("human").build();
        Player computer = new PlayerBuilder()
                .setName("computer")
                .setType("computer").build();
        Map<Player, Integer> map = new HashMap<Player, Integer>() {{
            put(human, 10);
            put(computer, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);

        gameEngine.play();

        verify(inputService).getGameMode();
    }

    @Test
    public void shouldInvokeOutputServiceToPrintTheGameResult() throws Exception {
        when(inputService.getNumberOfGames()).thenReturn(3);
        when(inputService.getGameMode()).thenReturn("computer");
        when(inputService.getInput("computer")).thenReturn(ROCK, PAPER);
        when(ruleEngine.apply(any(), any())).thenReturn(1,1, 1);
        Player human = new PlayerBuilder()
                .setName("Thiru")
                .setType("human").build();
        Player computer = new PlayerBuilder()
                .setName("computer")
                .setType("computer").build();
        Map<Player, Integer> map = new HashMap<Player, Integer>() {{
            put(human, 10);
            put(computer, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);

        gameEngine.play();

        verify(inputService, times(3)).getInput("computer");
        verify(inputService, times(3)).getInput("human");
        verify(outputService).printGameResult(human);
        verify(ruleEngine, times(3)).apply(any(), any());
    }
}
package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Weapons;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.mockito.Matchers;

import static com.webtrekk.rps.model.Weapons.*;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;
import static org.mockito.Matchers.contains;

public class InputServiceTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final TextFromStandardInputStream inputStream = emptyStandardInputStream();

    @Test
    public void shouldReturnRandomWeaponWhenTheInputPlayerIsComputer() throws Exception {
        InputService service = new InputService();

        Weapons weapon = service.getInput("computer");

        assertThat(weapon, anyOf(equalTo(ROCK), equalTo(PAPER), equalTo(SCISSOR)));
    }

    @Test
    public void shouldAskTheUserToProvideInputWhenTheTypeIsHuman() throws Exception {
        InputService service = new InputService();
        inputStream.provideLines("SCISSOR");

        service.getInput("human");

        assertThat(systemOutRule.getLog(), is("Please chose your weapon (rock/paper/scissor)\nUser chose: SCISSOR\n"));
    }

    @Test
    public void shouldReturnTheTypeOfWeaponAsScissorWhenTheUserInputIsScissor() throws Exception {
        InputService service = new InputService();

        inputStream.provideLines("SCISSOR");
        Weapons actualWeapon = service.getInput("human");

        assertThat(actualWeapon, is(SCISSOR));
    }

    @Test
    public void shouldReturnTheTypeOfWeaponAsPaperWhenTheUserInputIsPaper() throws Exception {
        InputService service = new InputService();

        inputStream.provideLines("PAPER");
        Weapons actualWeapon = service.getInput("human");

        assertThat(actualWeapon, is(PAPER));
    }

    @Test
    public void shouldReturnTypeOfWeaponForTheLowerCaseAndUpperCaseInputs() throws Exception {
        InputService service = new InputService();

        inputStream.provideLines("pApEr");
        Weapons actualWeapon = service.getInput("human");

        assertThat(actualWeapon, is(PAPER));
    }

    @Test
    public void shouldAskForInputAgainUntilAValidInputIsGiven() throws Exception {
        InputService service = new InputService();

        inputStream.provideLines("pAEr", "paper");
        Weapons actualWeapon = service.getInput("human");

        assertThat(systemOutRule.getLog(), is("Please chose your weapon (rock/paper/scissor)\nUser chose: PAER\nIncorrect input\nPlease chose your weapon (rock/paper/scissor)\nUser chose: PAPER\n"));
        assertThat(actualWeapon, is(PAPER));
    }

    @Test
    public void shouldFetchTheGameModeFormTheUser() throws Exception {
        inputStream.provideLines("single");
        InputService inputService = new InputService();

        String gameMode = inputService.getGameMode();

        assertThat(gameMode, is("single"));
        assertThat(systemOutRule.getLog(), is("Please enter the game mode (single or computer): \n"));
    }

    @Test
    public void shouldReturnTheNumberOfGamesBasedOnTheInputFromUser() throws Exception {
        inputStream.provideLines("3");
        InputService inputService = new InputService();

        int numberOfGames = inputService.getNumberOfGames();

        assertThat(numberOfGames, is(3));
    }

    @Test
    public void shouldReturnTheNameOfThePlayerFetchedFromTheConsole() throws Exception {
        inputStream.provideLines("John");
        InputService inputService = new InputService();

        String playerName = inputService.getPlayerName();

        assertThat(systemOutRule.getLog(), is("Enter the player name:\n"));
        assertThat(playerName, is("John"));
    }

    @Test
    public void shouldAcceptOnlyOddNumberAsItHandlesTheTieSituation() throws Exception {
        inputStream.provideLines("2", "3");
        InputService inputService = new InputService();

        int numberOfGames = inputService.getNumberOfGames();

        assertThat(numberOfGames, is(3));
        assertThat(systemOutRule.getLog(), containsString("Number of games should be a odd number."));
    }

    @Test
    public void shouldPromptAgainToFetchTheGameModeIfItIsNotSingleOrComputer() throws Exception {
        inputStream.provideLines("singld", "single");
        InputService inputService = new InputService();

        String gameMode = inputService.getGameMode();

        assertThat(gameMode, is("single"));
    }

    @Test
    public void shouldPromptAgainToFetchTheGameModeIfItIsNotSingleOrNotComputerAndShouldReturnTheInput() throws Exception {
        inputStream.provideLines("singld", "computer");
        InputService inputService = new InputService();

        String gameMode = inputService.getGameMode();

        assertThat(gameMode, is("computer"));
    }
}
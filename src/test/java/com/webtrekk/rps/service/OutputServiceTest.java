package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class OutputServiceTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Mock
    private PointTracker pointTracker;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldPrintTheWinnerNameOfTheGame() throws Exception {
        OutputService outputService = new OutputService(pointTracker);
        Player player = new PlayerBuilder().setName("Player name").build();

        outputService.printResult(player);

        assertThat(systemOutRule.getLog(), is("The winner of the round is Player name\n"));
    }

    @Test
    public void shouldPrintThePointsOfAllThePlayers() throws Exception {
        OutputService outputService = new OutputService(pointTracker);
        Player john = new PlayerBuilder().setName("John").build();
        Player jack = new PlayerBuilder().setName("Jack").build();
        Map<Player, Integer> map = new LinkedHashMap<Player, Integer>() {{
            put(jack, 9);
            put(john, 10);
        }};
        when(pointTracker.getScores()).thenReturn(map);
        Player player = new PlayerBuilder().setName("Player name").build();

        outputService.printResult(player);

        assertThat(systemOutRule.getLog(), is("The winner of the round is Player name\n"));
    }

    @Test
    public void shouldPrintGameResultAtTheEndOfTheGame() throws Exception {
        OutputService outputService = new OutputService(pointTracker);
        Player john = new PlayerBuilder().setName("John").build();
        Player jack = new PlayerBuilder().setName("Jack").build();
        Map<Player, Integer> map = new LinkedHashMap<Player, Integer>() {{
            put(john, 10);
            put(jack, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);

        outputService.printGameResult(john);

        assertThat(systemOutRule.getLog(), containsString("John score: 10\nJack score: 9\n"));
    }

    @Test
    public void shouldPrintWinnerOfTheEntireGame() throws Exception {
        OutputService outputService = new OutputService(pointTracker);
        Player john = new PlayerBuilder().setName("John").build();
        Player jack = new PlayerBuilder().setName("Jack").build();
        Map<Player, Integer> map = new LinkedHashMap<Player, Integer>() {{
            put(john, 10);
            put(jack, 9);
        }};
        when(pointTracker.getScores()).thenReturn(map);

        outputService.printGameResult(john);

        assertThat(systemOutRule.getLog(), containsString("Congrats John!! You are the winner\n"));
    }
}
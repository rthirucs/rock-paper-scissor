package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class PointTrackerTest {


    @Test
    public void shouldAddPlayerToTheMapWithScoreAsZeroWhenTheUserIsNotPresentInTheTracker() throws Exception {
        PointTracker pointTracker = new PointTracker();
        Player player = new PlayerBuilder().setName("John").build();

        pointTracker.incrementScore(player);

        assertThat(pointTracker.getScores().get(player), is(1));
    }

    @Test
    public void shouldIncrementTheScoreOfThePlayerIfThePlayerAlreadyExistsInTheList() throws Exception {
        PointTracker pointTracker = new PointTracker();
        Player player = new PlayerBuilder().setName("John").build();
        pointTracker.getScores().put(player, 2);

        pointTracker.incrementScore(player);

        assertThat(pointTracker.getScores().get(player), is(3));
    }

    @Test
    public void shouldClearAllTheEntriesInTheScores() throws Exception {
        PointTracker pointTracker = new PointTracker();
        Player player = new PlayerBuilder().setName("John").build();
        pointTracker.getScores().put(player, 2);

        pointTracker.resetScores();
        
        assertThat(pointTracker.getScores(), is(new HashMap<>()));
    }

    @Test
    public void shouldAddPlayersToScoresMapWithScoreAs0() throws Exception {
        PointTracker pointTracker = new PointTracker();
        Player firstPlayer = new PlayerBuilder().setName("John").build();
        Player secondPlayer = new PlayerBuilder().setName("Jack").build();

        pointTracker.addPlayers(firstPlayer, secondPlayer);

        Map<Player, Integer> scores = pointTracker.getScores();
        assertThat(scores.get(firstPlayer), is(0));
        assertThat(scores.get(secondPlayer), is(0));
    }
}
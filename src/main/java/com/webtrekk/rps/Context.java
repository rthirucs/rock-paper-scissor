package com.webtrekk.rps;

import com.webtrekk.rps.service.*;

import java.util.Arrays;
import java.util.List;

public class Context {

    private static Context context = new Context();

    private GameEngine gameEngine;
    private InputService inputService;
    private OutputService outputService;
    private RuleEngine ruleEngine;
    private SinglePlayerHandler singlePlayerHandler;
    private ComputerPlayersHandler computerPlayersHandler;
    private PointTracker pointTracker;

    public static Context getInstance() {
        return context;
    }

    private Context() {
    }


    public InputService inputService() {
        if (this.inputService == null)
            this.inputService = new InputService();
        return this.inputService;
    }

    public OutputService outputService() {
        if (this.outputService == null)
            this.outputService = new OutputService(pointTracker());
        return this.outputService;
    }

    public RuleEngine ruleEngine() {
        if (this.ruleEngine == null)
            this.ruleEngine = new RuleEngine();
        return this.ruleEngine;
    }

    public SinglePlayerHandler singlePlayerHandler() {
        if (this.singlePlayerHandler == null)
            this.singlePlayerHandler = new SinglePlayerHandler(inputService(), pointTracker());
        return singlePlayerHandler;
    }

    public ComputerPlayersHandler computerPlayersHandler() {
        if (this.computerPlayersHandler == null)
            this.computerPlayersHandler = new ComputerPlayersHandler(pointTracker());
        return computerPlayersHandler;
    }

    public List<GameHandler> handlers() {
        return Arrays.asList(singlePlayerHandler(), computerPlayersHandler());
    }

    public PointTracker pointTracker() {
        if (this.pointTracker == null)
            this.pointTracker = new PointTracker();
        return pointTracker;
    }

    public GameEngine gameEngine() {
        if (this.gameEngine == null)
            this.gameEngine = new GameEngine(inputService(), ruleEngine(), outputService(), pointTracker(), handlers());
        return this.gameEngine;
    }
}

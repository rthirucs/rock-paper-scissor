package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Player;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PointTracker {

    private Map<Player, Integer> scores = Collections.synchronizedMap(new HashMap<>());

    public void incrementScore(Player player) {
        scores.computeIfPresent(player, (k, v) -> v + 1);

        scores.putIfAbsent(player, 1);
    }

    public Map<Player, Integer> getScores() {
        return scores;
    }

    public void resetScores() {
        scores.clear();
    }

    public void addPlayers(Player... players) {
        for (Player player : players) {
            scores.put(player, 0);
        }
    }
}

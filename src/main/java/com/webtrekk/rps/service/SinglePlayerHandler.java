package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;

public class SinglePlayerHandler implements GameHandler {

    private static final String TYPE = "single";

    private final InputService inputService;
    private final PointTracker pointTracker;
    private Player firstPlayer;
    private Player secondPlayer;

    public SinglePlayerHandler(InputService inputService, PointTracker pointTracker) {
        this.inputService = inputService;
        this.pointTracker = pointTracker;
    }

    @Override
    public boolean canHandle(String type) {
        return TYPE.equals(type);
    }

    @Override
    public void initializePlayers() {
        String playerName = inputService.getPlayerName();
        firstPlayer = new PlayerBuilder()
                .setName(playerName)
                .setType("human").build();
        secondPlayer = new PlayerBuilder()
                .setName("Computer")
                .setType("computer").build();

        pointTracker.addPlayers(firstPlayer, secondPlayer);
    }

    @Override
    public Player getFirstPlayer() {
        return firstPlayer;
    }

    @Override
    public Player getSecondPlayer() {
        return secondPlayer;
    }
}

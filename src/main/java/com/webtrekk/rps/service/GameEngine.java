package com.webtrekk.rps.service;

import com.webtrekk.rps.Context;
import com.webtrekk.rps.model.Player;
import com.webtrekk.rps.model.Weapons;

import java.util.List;
import java.util.Map;

import static java.util.Collections.max;
import static java.util.Map.Entry.comparingByValue;

public class GameEngine {

    private final InputService inputService;
    private final RuleEngine ruleEngine;
    private final OutputService outputService;
    private final PointTracker pointTracker;
    private final List<GameHandler> handlers;

    public GameEngine(InputService inputService, RuleEngine ruleEngine, OutputService outputService,
                      PointTracker pointTracker, List<GameHandler> handlers) {
        this.inputService = inputService;
        this.ruleEngine = ruleEngine;
        this.outputService = outputService;
        this.pointTracker = pointTracker;
        this.handlers = handlers;
    }

    public void play() {
        int numberOfGames = inputService.getNumberOfGames();
        pointTracker.resetScores();
        GameHandler handler = getHandler();
        handler.initializePlayers();

        for (int index = 0; index < numberOfGames; index++) {
            play(handler.getFirstPlayer(), handler.getSecondPlayer());
        }

        Map<Player, Integer> scores = pointTracker.getScores();
        Player winner = max(scores.entrySet(), comparingByValue()).getKey();

        outputService.printGameResult(winner);
    }

    private void play(Player firstPlayer, Player secondPlayer) {
        while (true) {
            Weapons humanWeapon = inputService.getInput(firstPlayer.getType());
            Weapons computerWeapon = inputService.getInput(secondPlayer.getType());

            int result = ruleEngine.apply(humanWeapon, computerWeapon);

            if (result > 0) {
                pointTracker.incrementScore(firstPlayer);
                outputService.printResult(firstPlayer);
                return;
            } else if (result < 0) {
                pointTracker.incrementScore(secondPlayer);
                outputService.printResult(secondPlayer);
                return;
            } else {
                outputService.tieMessage();
            }
        }
    }

    private GameHandler getHandler() {
        String gameMode = inputService.getGameMode();
        return handlers.stream()
                .filter(handler -> handler.canHandle(gameMode))
                .findFirst()
                .get();
    }

}

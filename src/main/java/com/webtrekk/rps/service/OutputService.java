package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Player;

import java.util.Map;

public class OutputService {

    private final PointTracker pointTracker;

    public OutputService(PointTracker pointTracker) {
        this.pointTracker = pointTracker;
    }

    public void printResult(Player winner) {
        System.out.println("The winner of the round is " + winner.getName());
    }

    public void printGameResult(Player winner) {
        Map<Player, Integer> scores = pointTracker.getScores();
        scores.forEach((k, v) -> System.out.println(k.getName() + " score: " + v));
        System.out.println("Congrats " + winner.getName() + "!! You are the winner");
    }

    public void tieMessage() {
        System.out.println("Its tie!! This is not counted as a round. Play again");
    }
}

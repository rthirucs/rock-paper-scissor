package com.webtrekk.rps.service;

import com.webtrekk.rps.builders.PlayerBuilder;
import com.webtrekk.rps.model.Player;

public class ComputerPlayersHandler implements GameHandler {

    private final PointTracker pointTracker;
    private Player firstPlayer;
    private Player secondPlayer;

    public ComputerPlayersHandler(PointTracker pointTracker) {
        this.pointTracker = pointTracker;
    }

    @Override
    public boolean canHandle(String type) {
        return "computer".equals(type);
    }

    @Override
    public void initializePlayers() {
        firstPlayer = new PlayerBuilder().setType("computer").setName("Computer 1").build();
        secondPlayer = new PlayerBuilder().setType("computer").setName("Computer 2").build();
        pointTracker.addPlayers(firstPlayer, secondPlayer);
    }

    @Override
    public Player getFirstPlayer() {
        return firstPlayer;
    }

    @Override
    public Player getSecondPlayer() {
        return secondPlayer;
    }
}

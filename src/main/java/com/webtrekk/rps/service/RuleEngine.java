package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Weapons;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.webtrekk.rps.model.Weapons.*;
import static java.util.Arrays.asList;

public class RuleEngine {

    private static Map<Weapons, List<Weapons>> map;

    static {
        map = new HashMap<>();
        map.put(ROCK, asList(SCISSOR));
        map.put(SCISSOR, asList(PAPER));
        map.put(PAPER, asList(ROCK));
    }

    public int apply(Weapons firstPlayerWeapon, Weapons secondPlayerWeapon) {
        List<Weapons> loosingWeapons = map.get(firstPlayerWeapon);
        if (firstPlayerWeapon == secondPlayerWeapon)
            return 0;

        return loosingWeapons.contains(secondPlayerWeapon) ? 1 : -1;
    }
}

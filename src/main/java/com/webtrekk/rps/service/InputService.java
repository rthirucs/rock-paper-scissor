package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Weapons;

import java.util.Random;
import java.util.Scanner;

public class InputService {

    public Weapons getInput(String type) {
        if (type.equals("computer")) {
            return randomWeapon();
        }
        return userInput();
    }

    public String getGameMode() {
        System.out.println("Please enter the game mode (single or computer): ");
        Scanner scanner = new Scanner(System.in);
        String gameMode = scanner.next();
        if (!gameMode.equals("single") && !gameMode.equals("computer"))
            return getGameMode();
        return gameMode;
    }

    private Weapons randomWeapon() {
        Weapons[] weapons = Weapons.values();
        Weapons weapon = weapons[new Random().nextInt(weapons.length)];
        System.out.println("Computer chose: " + weapon);
        return weapon;
    }

    private Weapons userInput() {
        while (true) {
            System.out.println("Please chose your weapon (rock/paper/scissor)");
            Scanner scanner = new Scanner(System.in);
            try {
                String input = scanner.next().toUpperCase();
                System.out.println("User chose: " + input);
                return Weapons.valueOf(input);
            } catch (IllegalArgumentException e) {
                System.out.println("Incorrect input");
            }
        }
    }

    public int getNumberOfGames() {
        System.out.println("Enter number of rounds in odd number.");
        Scanner scanner = new Scanner(System.in);
        int numberOfGames = scanner.nextInt();
        if (numberOfGames % 2 == 0) {
            System.out.println("Number of games should be a odd number.");
            return getNumberOfGames();
        }
        return numberOfGames;
    }

    public String getPlayerName() {
        System.out.println("Enter the player name:");
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }
}

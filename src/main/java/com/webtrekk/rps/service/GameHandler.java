package com.webtrekk.rps.service;

import com.webtrekk.rps.model.Player;

public interface GameHandler {

    boolean canHandle(String type);

    void initializePlayers();

    Player getFirstPlayer();

    Player getSecondPlayer();
}

package com.webtrekk.rps.model;

public enum Weapons {
    ROCK,
    PAPER,
    SCISSOR
}

package com.webtrekk.rps;

import com.webtrekk.rps.service.GameEngine;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        GameEngine gameEngine = Context.getInstance().gameEngine();
        System.out.println("Lets Play Rock Paper Scissor!! Press Ctrl + c to exit");
        String input;
        do {
            gameEngine.play();
            System.out.println("Do you want to play another game?(yes/no)");
            Scanner scanner = new Scanner(System.in);
            input = scanner.next();
        } while (input.equals("yes"));
    }
}

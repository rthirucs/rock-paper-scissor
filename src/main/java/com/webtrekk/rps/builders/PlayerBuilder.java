package com.webtrekk.rps.builders;

import com.webtrekk.rps.model.Player;

public class PlayerBuilder {
    private String name;
    private String type;

    public PlayerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PlayerBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public Player build() {
        return new Player(name, type);
    }
}